import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { MapMasterComponent } from './map-master/map-master.component';
import { MapSlaveComponent } from './map-slave/map-slave.component';


@NgModule({
  declarations: [
    AppComponent,
    MapMasterComponent,
    MapSlaveComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBTHnTltwrUq6Hb_soEU4qekJaXqfCR-hU'      
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
