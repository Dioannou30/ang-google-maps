import { Component, OnInit } from "@angular/core";
import { IBranch, IPoint, IDistance } from "../models/models";
import { branches } from "../branches";
import { MouseEvent as AGMMouseEvent, LatLngLiteral } from "@agm/core";
import { MapDataService } from "../map-data.service";

@Component({
  selector: "map-master",
  styleUrls: ["./map-master.component.css"],
  templateUrl: "./map-master.component.html",
})
export class MapMasterComponent implements OnInit {
  latitude: number = 37.97128143577793;
  longitude: number = 23.73416373;
  mapType = "roadmap";
  zoom: number = 8;
  maxWidth: number = 500;
  markers: IBranch[] = [];
  iconUrl: string = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
  cursor: IPoint;
  customPoints: IPoint[] = [];
  paths = [];
  showPoly: boolean = false;

  constructor(private mapDataService: MapDataService) {}

  ngOnInit(): void {
    this.markers = branches;
    this.markers.forEach((x) => {
      this.paths.push({ lat: parseFloat(x.Lat.toString()), lng: parseFloat(x.Lon.toString()) });
    });
  }

  delete() {
    this.markers = this.markers.filter((x) => x.isCustom == false);
    this.mapDataService.deleteAllPoints();
  }

  showPolygon() {
    this.showPoly = !this.showPoly;
  }

  dropMarker($event: AGMMouseEvent) {
    let _temp = <IBranch>{
      Lat: $event.coords.lat.toString(),
      Lon: $event.coords.lng.toString(),
      isCustom: true,
    };
    // Add too map
    this.markers.push(_temp);
    //Center map
    this.longitude = $event.coords.lng;
    this.latitude = $event.coords.lat;
    //Set and push for slave map
    let _point = <IPoint>{ Lon: $event.coords.lng, Lat: $event.coords.lat };
    this.mapDataService.addPoint(_point);
    this.calculateAllDistances(_point);
  }

  calculateAllDistances(point: IPoint) {
    let lowestDistance = <IDistance>{ distance: Number.POSITIVE_INFINITY };
    let closest_branch: IBranch = null;
    for (let i = 0; i < this.markers.length; i++) {
      let _branch = this.markers[i];
      if (_branch.isCustom) continue;
      let _distance = this.getDistanceFromLatLonInKm(point, _branch);
      if (_distance < lowestDistance.distance) {
        lowestDistance.distance = _distance;
        lowestDistance.point = _branch;
        closest_branch = _branch;
      }
    }

    closest_branch.Lat = parseFloat(closest_branch.Lat.toString());
    closest_branch.Lon = parseFloat(closest_branch.Lon.toString());
    this.mapDataService.addPoint(closest_branch);
  }

  selectMarker(i: number) {
    this.markers[i].displayInfoWindow = !this.markers[i].displayInfoWindow;
  }

  getDistanceFromLatLonInKm(customPoint: IPoint, branchPoint: IBranch) {
    //https://stackoverflow.com/questions/18883601/function-to-calculate-distance-between-two-coordinates
    let lat1: number = customPoint.Lat;
    let lon1: number = customPoint.Lon;
    let lat2: number = parseFloat(branchPoint.Lat.toString());
    let lon2: number = parseFloat(branchPoint.Lon.toString());
    const R = 6371; // Radius of the earth in km
    let dLat = this.deg2rad(lat2 - lat1);
    let dLon = this.deg2rad(lon2 - lon1);
    let a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) *
        Math.cos(this.deg2rad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg: number) {
    return deg * (Math.PI / 180);
  }
}
