export interface IBranch {
    Id?: string;
    Name?: string;
    SearchName?: string;
    hasGold?: boolean;
    hasSecurityDeposit?: boolean;
    extHours?: boolean;
    pwdsSupport?: boolean;
    hasKas?: boolean;
    services?: string[];
    specialServices?: string[];
    Countryname?: string;
    CountryId?: string;
    Regionname?: string;
    RegionId?: string;
    Cityname?: string;
    CityId?: string;
    AllocationId?: string;
    AccessId?: string;
    AddressMain?: string;
    AddressSuppl?: string;
    FacilityId?: string;
    OrganizationId?: string;
    Telephone?: string;
    Fax?: string;
    email?: string;
    hours?: string;
    atmHours?: string;
    Zip?: string;
    Lat?: string | number;
    Lon?: string | number;
    Distance?: string;
    alphaExpress?: boolean;
    displayInfoWindow?: boolean;
    isCustom?: boolean      
}

export interface IPoint {
    Lat: number,
    Lon: number
}

export interface IDistance {
    distance: number;
    point: IBranch;
}