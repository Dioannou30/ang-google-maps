import { Injectable } from '@angular/core';
import { IBranch } from './models/models';
import { removeSummaryDuplicates } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class MapDataService {

  storage: IBranch[] = [];
  constructor() { }
  
  getPoints(): IBranch[] {
    return this.storage;
  }

  addPoint(point: IBranch): void {
    if(this.storage.length == 2){
      return;
    }else{
      this.storage.push(point);
    }    
  }
  
  deleteAllPoints(): void {
    this.storage = [];
  }

}
