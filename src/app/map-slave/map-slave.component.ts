import { Component, OnInit } from '@angular/core';
import { MapDataService } from '../map-data.service';
import { IPoint } from '../models/models';

@Component({
  selector: 'map-slave',
  templateUrl: './map-slave.component.html',
  styleUrls: ['./map-slave.component.css']
})
export class MapSlaveComponent implements OnInit {
  latitude: number = 37.97128143577793;
  longitude: number = 23.73416373;
  mapType = "roadmap";
  zoom: number = 8;
  maxWidth: number = 500;

  public get markers() {
    let data = this.mapDataService.getPoints();    
    return data;
  }

  constructor(private mapDataService: MapDataService) { }

  ngOnInit(): void {
  }

  selectMarker(i: number) {
    this.markers[i].displayInfoWindow = !this.markers[i].displayInfoWindow;
  }
}
